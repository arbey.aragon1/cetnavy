const STTService = require("./STTService");
const path = require("path");
const { from, of } = require("rxjs");
const { map, tap, switchMap, delay } = require("rxjs/operators");

sttService = new STTService();

sttService
    .speech_to_text(path.join(__dirname + "/public", "myfile.wav"))
    .subscribe(
        (str) => {
            console.log(str);
            var code = "";
            if (str.includes("one")) {
                code = "1";
            } else if (str.includes("two")) {
                code = "2";
            } else if (str.includes("three")) {
                code = "3";
            } else if (str.includes("four")) {
                code = "4";
            } else if (str.includes("five")) {
                code = "5";
            } else if (str.includes("six")) {
                code = "6";
            } else if (str.includes("seven")) {
                code = "7";
            } else if (str.includes("eight")) {
                code = "8";
            } else if (str.includes("nine")) {
                code = "9";
            } else if (str.includes("ten")) {
                code = "10";
            } else if (str.includes("eleven")) {
                code = "11";
            } else if (str.includes("twelve")) {
                code = "12";
            } else {
                code = str;
            }
            console.log(code);
        },
        (err) => console.log("error:", err)
    );
/** */
