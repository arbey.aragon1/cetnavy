"use strict";
const SpeechToTextV1 = require("ibm-watson/speech-to-text/v1");
const { IamAuthenticator } = require("ibm-watson/auth");
const fs = require("fs");
const { from, Observable, of } = require("rxjs");
const { map, switchMap, tap } = require("rxjs/operators");
var FfmpegCommand = require("fluent-ffmpeg");
var command = new FfmpegCommand();
var ffmpeg = require("fluent-ffmpeg");
var credentials = require("./credentials");

module.exports = class STTService {
    constructor() {
        const speech_to_text_credentials =
            credentials.speech_to_text_credentials;

        this.speechToText = new SpeechToTextV1({
            authenticator: new IamAuthenticator({
                apikey: speech_to_text_credentials.apikey,
            }),
            url: speech_to_text_credentials.url,
        });
        console.log(speech_to_text_credentials);
    }

    convert_stereo(input, output) {
        return new Observable((subscriber) => {
            ffmpeg(input)
                .output(output)
                .on("end", function () {
                    console.log("conversion ended");
                    subscriber.next(output);
                    subscriber.complete();
                })
                .on("error", function (err) {
                    console.log("error: ", err.code, err.msg);
                    subscriber.error(err);
                    subscriber.complete();
                })
                .run();
        });
    }

    speech_to_text(filePath) {
        var startTime = Date.now();

        var path = filePath;
        var path2 = filePath.replace(".wav", ".mp3");

        return of(filePath).pipe(
            switchMap(() => {
                return this.convert_stereo(path, path2);
            }),

            map(() => ({
                audio: fs.createReadStream(path2),
                contentType: "audio/mp3",
            })),

            switchMap((parm) => from(this.speechToText.recognize(parm))),

            map((speechRecognitionResults) => {
                const data = speechRecognitionResults;
                console.log(JSON.stringify(speechRecognitionResults, null, 2));
                var elapsedTime = Date.now() - startTime;
                console.log(elapsedTime / 1000);
                return data["result"]["results"][0]["alternatives"][0][
                    "transcript"
                ];
            })
        );
    }
};
