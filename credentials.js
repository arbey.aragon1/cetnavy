const speech_to_text_credentials = {
    apikey: "B91p5Gpnz1j9QV_xnWzRug9IylEb-STkU3nKg9jWmGRR",
    iam_apikey_description:
        "Auto-generated for key 76369f43-f571-4f8e-8ab5-0d100a950fa9",
    iam_apikey_name: "Auto-generated service credentials",
    iam_role_crn: "crn:v1:bluemix:public:iam::::serviceRole:Manager",
    iam_serviceid_crn:
        "crn:v1:bluemix:public:iam-identity::a/0ddb689dea554db998a10f34ffc10807::serviceid:ServiceId-609c47d1-56b3-43ae-8747-03725f613991",
    url:
        "https://api.us-south.speech-to-text.watson.cloud.ibm.com/instances/6538b36a-73f5-424a-b035-86487266e8e9",
};

const text_to_speech_credentials = {
    apikey: "lvoE8AHIerjgzfmGb0EkRF0F2KkWXrJIRLGErLssZcWs",
    iam_apikey_description:
        "Auto-generated for key d82a2c44-16d9-4a35-8ad4-96de06f6bfd3",
    iam_apikey_name: "Auto-generated service credentials",
    iam_role_crn: "crn:v1:bluemix:public:iam::::serviceRole:Manager",
    iam_serviceid_crn:
        "crn:v1:bluemix:public:iam-identity::a/0ddb689dea554db998a10f34ffc10807::serviceid:ServiceId-0c53f76b-0054-4d1b-aa2c-3013974af230",
    url:
        "https://api.us-south.text-to-speech.watson.cloud.ibm.com/instances/23383b5f-585e-4260-83f9-9cd77f155164",
};

const watson_assistant_credentials = {
    apikey: "O3rEya9OKGaPWI_rBeEq9j4O51lIBLWFEcRogB2PHdLo",
    iam_apikey_description:
        "Auto-generated for key a40f409d-b918-4f4b-bbe9-d8dc3f16269d",
    iam_apikey_name: "Auto-generated service credentials",
    iam_role_crn: "crn:v1:bluemix:public:iam::::serviceRole:Manager",
    iam_serviceid_crn:
        "crn:v1:bluemix:public:iam-identity::a/0ddb689dea554db998a10f34ffc10807::serviceid:ServiceId-35d647df-1c7e-4712-a283-db9bfec8dbfa",
    url:
        "https://api.eu-gb.assistant.watson.cloud.ibm.com/instances/f423398d-f29c-4194-801e-f13759c01b75",
    assistant_id: "7fdfb402-2db2-43aa-ab46-e6c48408700a",
    assistant_url:
        "https://api.eu-gb.assistant.watson.cloud.ibm.com/instances/f423398d-f29c-4194-801e-f13759c01b75/v2/assistants/7fdfb402-2db2-43aa-ab46-e6c48408700a/sessions",
    assistant_name: "Justin Beaver",
};

module.exports = Object.freeze({
    speech_to_text_credentials: speech_to_text_credentials,
    text_to_speech_credentials: text_to_speech_credentials,
    watson_assistant_credentials: watson_assistant_credentials,
});
