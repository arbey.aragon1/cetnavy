"use strict";
const express = require("express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);
const fs = require("fs");
var multer = require("multer");
const bodyParser = require("body-parser");
const { IamAuthenticator } = require("ibm-watson/auth");
const path = require("path");
const os = require("os");
const QRReader = require("qrcode-reader");

const jimp = require("jimp");
const { Console } = require("console");

const STTService = require("./STTService");
const { from, of } = require("rxjs");
const { map, tap, switchMap, delay } = require("rxjs/operators");

const sttService = new STTService();

const mainPath = __dirname;

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __dirname + "/public");
    },
    filename: function (req, file, cb) {
        console.log(file);
        cb(null, file.originalname);
    },
});

const upload = multer({ storage: storage });

async function ReadQR() {
    const pathFiles = path.join(__dirname, "public", "image.png");

    console.log(pathFiles);
    const img = await jimp.read(fs.readFileSync(pathFiles));

    const qr = new QRReader();

    const value = await new Promise((resolve, reject) => {
        qr.callback = (err, v) => (err != null ? reject(err) : resolve(v));
        qr.decode(img.bitmap);
    });

    console.log(value);
    console.log(value.result);
    return value.result;
}

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

class AppServer {
    start = 0;
    text = "";
    constructor() {
        this.start = Date.now();
        this.ConfigureFiles();
        this.ConfigureEndPoints();
        this.Run();
    }

    ConfigureFiles() {
        const self = this;
        app.post(
            "/audio-upload",
            upload.single("uploadFile"),
            (req, res, next) => {
                try {
                    const file = req.file;
                    if (!file) {
                        res.status(400).json({
                            status: "failed",
                            code: "400",
                            message: "Please upload file",
                        });
                    }

                    sttService
                        .speech_to_text(
                            path.join(__dirname + "/public", "myfile.wav")
                        )
                        .subscribe(
                            (str) => {
                                console.log(str);
                                var code = "";
                                if (str.includes("zero")) {
                                    code = "0";
                                } else if (str.includes("one")) {
                                    code = "1";
                                } else if (str.includes("two")) {
                                    code = "2";
                                } else if (str.includes("three")) {
                                    code = "3";
                                } else if (str.includes("four")) {
                                    code = "4";
                                } else if (str.includes("five")) {
                                    code = "5";
                                } else if (str.includes("six")) {
                                    code = "6";
                                } else if (str.includes("seven")) {
                                    code = "7";
                                } else if (str.includes("eight")) {
                                    code = "8";
                                } else if (str.includes("nine")) {
                                    code = "9";
                                } else if (str.includes("ten")) {
                                    code = "10";
                                } else if (str.includes("eleven")) {
                                    code = "11";
                                } else if (str.includes("twelve")) {
                                    code = "12";
                                } else {
                                    code = str;
                                }
                                console.log(code);
                                res.status(200).json({
                                    status: "success",
                                    code: "200",
                                    message: code,
                                });
                            },
                            (err) => console.log("error:", err)
                        );
                } catch (error) {
                    console.log(error.message);
                    res.status(200).json({
                        status: "failed",
                        code: "500",
                        message: error.message,
                    });
                }
            }
        );

        app.post(
            "/image-upload",
            upload.single("uploadFile"),
            (req, res, next) => {
                try {
                    const file = req.file;
                    if (!file) {
                        res.status(400).json({
                            status: "failed",
                            code: "400",
                            message: "Please upload file",
                        });
                    }

                    ReadQR()
                        .then((value) => {
                            console.log("Value");
                            console.log(value);
                            res.status(200).json({
                                status: "success",
                                code: "200",
                                message: "" + value,
                            });
                        })
                        .catch((error) => console.error(error.stack));
                } catch (error) {
                    console.log(error.message);
                    res.status(200).json({
                        status: "failed",
                        code: "500",
                        message: error.message,
                    });
                }
            }
        );
    }

    GetIPAddress() {
        var interfaces = os.networkInterfaces();
        for (var devName in interfaces) {
            var iface = interfaces[devName];

            for (var i = 0; i < iface.length; i++) {
                var alias = iface[i];
                if (
                    alias.family === "IPv4" &&
                    alias.address !== "127.0.0.1" &&
                    !alias.internal
                )
                    return alias.address;
            }
        }
        return "0.0.0.0";
    }

    ConfigureEndPoints() {
        const self = this;
        app.get("/test", function (req, res) {
            res.send("test");
        });

        app.get("/audio-download", function (req, res) {
            const file = path.join(
                constants.TEMPORAL_FOLDER_PATH,
                "audioJustin.wav"
            );
            res.download(file);
        });
    }

    Run() {
        const localIP = this.GetIPAddress();

        http.listen(3000, function () {
            console.log("Listening on " + localIP + ":3000");
        });
    }
}

var serv = new AppServer();
