const fs = require("fs");
var QRCode = require("qrcode");

QRCode.toDataURL("CETREAL", { type: "terminal" }, function (err, base64String) {
    let base64Image = base64String.split(";base64,").pop();
    console.log(base64Image);
    fs.writeFile("image.png", base64Image, { encoding: "base64" }, function (
        err
    ) {
        console.log("File created");
    });
});
